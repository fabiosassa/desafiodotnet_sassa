﻿using System;

namespace Builders.DotNetCore.Services.Enums
{
    public enum FailureMessageType
    {
        Critical   ,
        Business   ,
        Validation ,
        NotFound
    }
}