﻿using System;
using System.Runtime.Serialization;

using Builders.DotNetCore.Services.Enums;

namespace Builders.DotNetCore.Services
{
    [DataContract]
    public class FailureMessage
    {
        [DataMember]
        public FailureMessageType Type { get; }
        [DataMember]
        public string             Text { get; }

        public FailureMessage(FailureMessageType type, string text)
        {
            Type = type;
            Text = text;
        }
    }
}