﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

using Builders.DotNetCore.Services.Enums;

namespace Builders.DotNetCore.Services.Http
{
    [DataContract]
    public abstract class SSTIResponse
    {
        private readonly List<FailureMessage> _messages
                   = new List<FailureMessage>();

        public HttpStatusCode StatusCode { get; private set; } = HttpStatusCode.OK;
        [DataMember]
        public bool           IsValid    { get; private set; } = true;
        [DataMember]
        public ReadOnlyCollection<FailureMessage> Messages => _messages.AsReadOnly();

        public void AddFailureMessage    (FailureMessageType type, string message)
        {
            IsValid = false;

            switch (type)
            {
                case FailureMessageType.Critical:
                    StatusCode = HttpStatusCode.InternalServerError;
                    break;
                case FailureMessageType.Validation:
                    StatusCode = HttpStatusCode.BadRequest;
                    break;
                case FailureMessageType.NotFound:
                    StatusCode = HttpStatusCode.NotFound;
                    break;
                default:
                    StatusCode = HttpStatusCode.Conflict;
                    break;
            }

            _messages.Add(new FailureMessage(type, message));
        }
    }
}