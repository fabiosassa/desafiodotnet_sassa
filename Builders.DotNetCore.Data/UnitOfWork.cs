﻿using System;
using System.Threading.Tasks;

namespace Builders.DotNetCore.Data
{
    public interface IUnitOfWork : IDisposable
    {
        Repository<TEntity> GetRepository<TEntity>() where TEntity : class;
        void FlushSession();
    }
}
