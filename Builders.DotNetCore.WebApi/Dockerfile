#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat

FROM microsoft/aspnetcore:2.0-nanoserver-sac2016 AS base
WORKDIR /app
EXPOSE 80

FROM microsoft/aspnetcore-build:2.0-nanoserver-sac2016 AS build
WORKDIR /src
COPY ["Builders.DotNetCore.WebApi/Builders.DotNetCore.WebApi.csproj", "Builders.DotNetCore.WebApi/"]
COPY ["Builders.DotNetCore.Infrastructure/Builders.DotNetCore.Infrastructure.csproj", "Builders.DotNetCore.Infrastructure/"]
COPY ["Builders.DotNetCore.Application.Impl/Builders.DotNetCore.Application.Impl.csproj", "Builders.DotNetCore.Application.Impl/"]
COPY ["Builders.DotNetCore.Application/Builders.DotNetCore.Application.csproj", "Builders.DotNetCore.Application/"]
COPY ["Builders.DotNetCore.Core/Builders.DotNetCore.Services.csproj", "Builders.DotNetCore.Core/"]
COPY ["Builders.DotNetCore.Data/Builders.DotNetCore.Data.csproj", "Builders.DotNetCore.Data/"]
COPY ["Builders.DotNetCore.Domain/Builders.DotNetCore.Domain.csproj", "Builders.DotNetCore.Domain/"]
COPY ["Builders.DotNetCore.Data.Mongo/Builders.DotNetCore.Data.Mongo.csproj", "Builders.DotNetCore.Data.Mongo/"]
RUN dotnet restore "Builders.DotNetCore.WebApi/Builders.DotNetCore.WebApi.csproj"
COPY . .
WORKDIR "/src/Builders.DotNetCore.WebApi"
RUN dotnet build "Builders.DotNetCore.WebApi.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Builders.DotNetCore.WebApi.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Builders.DotNetCore.WebApi.dll"]