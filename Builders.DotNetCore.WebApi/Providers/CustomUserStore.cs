﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;
using System.Threading;
using Builders.DotNetCore.Data;
using Builders.DotNetCore.Infrastructure;

namespace Builders.DotNetCore.WebApi.Providers
{
    public class CustomUserStore : IUserStore<Usuario>, 
        IUserPasswordStore<Usuario>, IUserEmailStore<Usuario>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomUserStore(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        public async Task<IdentityResult> CreateAsync(Usuario user, 
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));

            await _unitOfWork.GetRepository<Usuario>().AddAsync(user);

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> DeleteAsync(Usuario user, 
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));

            await Task.Run(() => _unitOfWork.GetRepository<Usuario>().Remove(user));

            return IdentityResult.Success;
        }

        public void Dispose()
        {
        }

        public Task<Usuario> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (normalizedEmail.IsNull()) throw new ArgumentNullException(nameof(normalizedEmail));

            var usuario = _unitOfWork.GetRepository<Usuario>().FirstOrDefaultAsync(u => u.Email == normalizedEmail);

            return usuario;
        }

        public async Task<Usuario> FindByIdAsync(string userId, 
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (userId.IsNull()) throw new ArgumentNullException(nameof(userId));

            return await _unitOfWork
                .GetRepository<Usuario>()
                .FindAsync(userId);
        }

        public async Task<Usuario> FindByNameAsync(string userName, 
            CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (userName.IsNull()) throw new ArgumentNullException(nameof(userName));

            return await _unitOfWork
                .GetRepository<Usuario>()
                .FirstOrDefaultAsync(u => u.NormalizedUserName == userName);
        }

        public Task<string> GetEmailAsync(Usuario user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(Usuario user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(user.EmailConfirmed);
        }

        public Task<string> GetNormalizedEmailAsync(Usuario user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(user.NormalizedUserName);
        }

        public Task<string> GetNormalizedUserNameAsync(Usuario user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(user.NormalizedUserName);
        }

        public Task<string> GetPasswordHashAsync(Usuario user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.PasswordHash);
        }

        public Task<string> GetUserIdAsync(Usuario user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.Id);
        }

        public Task<string> GetUserNameAsync(Usuario user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.UserName);
        }

        public Task<bool> HasPasswordAsync(Usuario user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));

            return Task.FromResult(!string.IsNullOrEmpty(user.PasswordHash));
        }

        public Task SetEmailAsync(Usuario user, string email, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));
            if (email.IsNull()) throw new ArgumentNullException(nameof(email));

            return Task.FromResult(user.Email = email);
        }

        public Task SetEmailConfirmedAsync(Usuario user, bool confirmed, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));

            return Task.FromResult(user.EmailConfirmed = confirmed);
        }

        public Task SetNormalizedEmailAsync(Usuario user, string normalizedEmail, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(user.NormalizedUserName = normalizedEmail);
        }

        public Task SetNormalizedUserNameAsync(Usuario user, string normalizedName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));
            if (normalizedName.IsNull()) throw new ArgumentNullException(nameof(normalizedName));
            
            return Task.FromResult(user.NormalizedUserName = normalizedName);
        }

        public Task SetPasswordHashAsync(Usuario user, string passwordHash, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));
            if (passwordHash.IsNull()) throw new ArgumentNullException(nameof(passwordHash));
            
            return Task.FromResult(user.PasswordHash = passwordHash);
        }

        public Task SetUserNameAsync(Usuario user, string userName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));
            if (userName.IsNull()) throw new ArgumentNullException(nameof(userName));

            return Task.FromResult(user.UserName = userName);
        }

        public Task<IdentityResult> UpdateAsync(Usuario user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user.IsNull()) throw new ArgumentNullException(nameof(user));

            _unitOfWork.GetRepository<Usuario>().Update(user);

            return Task.FromResult(IdentityResult.Success);
        }
    }
}
