﻿using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

using Builders.DotNetCore.Services.Http;

namespace Builders.DotNetCore.WebApi.Extensions
{
    public static class HttpHelper
    {
        public static ActionResult Resolve(this SSTIResponse response)
        {
            return new ObjectResult(response) { StatusCode = (int?)response.StatusCode };
        }

        public static string GetRouteValue(this RouteData routeData, string key)
        {
            return routeData.Values[key].ToString();
        }
    }
}