﻿using System;

using Microsoft.AspNetCore.Mvc;

using Builders.DotNetCore.WebApi.Extensions;

using Builders.DotNetCore.Application.Trees;
using Builders.DotNetCore.Application.Trees.Messages;

namespace Builders.DotNetCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TreesController : Controller
    {
        private readonly ITreeService _service;

        public TreesController(ITreeService service)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _service.Select();

            return result.Resolve();
        }

        [HttpPost]
        public IActionResult Post([FromBody]TreeInsertRequest request)
        {
            var result = _service.Insert(request);

            return result.Resolve();
        }

        [HttpPatch, Route("{treeId}")]
        public IActionResult Patch([FromBody]TreeUpdateRequest request)
        {
            request.TreeId = RouteData.GetRouteValue("treeId");

            var result = _service.Update(request);

            return result.Resolve();
        }
    }
}