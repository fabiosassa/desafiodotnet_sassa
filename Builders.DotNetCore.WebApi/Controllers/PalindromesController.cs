﻿using System;

using Microsoft.AspNetCore.Mvc;

using Builders.DotNetCore.Application.Palindromes;
using Builders.DotNetCore.Application.Palindromes.Messages;

namespace Builders.DotNetCore.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class PalindromesController : Controller
    {
        private readonly IPalindromeService _service;

        public PalindromesController(IPalindromeService service)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        [HttpGet, Route("isPalindrome")]
        public IActionResult Get([FromQuery]PalindromeRequest request)
        {
            var result  = _service.CheckWhetherPalindrome(request);

            if (result.IsValid)
            {
                return new ObjectResult(new { result.Word, result.IsPalindrome }) { StatusCode = (int?)result.StatusCode };
            }

            return new ObjectResult(result.Messages[0].Text) { StatusCode = (int?)result.StatusCode };
        }
    }
}