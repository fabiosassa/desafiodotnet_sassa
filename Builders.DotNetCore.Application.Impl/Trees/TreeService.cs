﻿using System;
using System.Linq;
using System.Collections.Generic;

using Builders.DotNetCore.Data;
using Builders.DotNetCore.Services.Enums;

using Builders.DotNetCore.Domain;

using Builders.DotNetCore.Application.Trees;
using Builders.DotNetCore.Application.Trees.Messages;

using Builders.DotNetCore.Application.Impl.Trees;

namespace Builders.DotNetCore.Application.Impl.Palindromes
{
    public class TreeService : ITreeService
    {
        private readonly Repository<Tree> _repository;
        private readonly ITreeServiceValidator _validator;

        public TreeService(IUnitOfWork uow, ITreeServiceValidator validator)
        {
            if (null == uow)
            {
                throw new ArgumentNullException(nameof(uow));
            }

            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
            _repository = uow.GetRepository<Tree>();
        }

        public TreeSelectResponse Select()
        {
            var response = new TreeSelectResponse();

            try
            {
                var list = _repository.ToList();

                if (list.Any())
                {
                    response.Trees = list.Select(i => i.Map()).ToList();
                }
                else
                {
                    response.AddFailureMessage(FailureMessageType.NotFound, "No tree was found");
                }
            }
            catch (Exception uhEx)
            {
                response.AddFailureMessage(FailureMessageType.Critical, uhEx.Message);
            }

            return response;
        }

        public TreeInsertResponse Insert(TreeInsertRequest request)
        {
            var response = _validator.Validate(request);

            if (!response.IsValid)
            {
                return response;
            }

            try
            {
                var tree = request.Map();

                _repository.Add(tree);

                response.Tree = tree.Map();
            }
            catch (Exception uhEx)
            {
                response.AddFailureMessage(FailureMessageType.Critical, uhEx.Message);
            }

            return response;
        }

        public TreeUpdateResponse Update(TreeUpdateRequest request)
        {
            var response = _validator.Validate(request);

            if (!response.IsValid)
            {
                return response;
            }

            try
            {
                var tree = _repository.Find(request.TreeId);

                if (null == tree)
                {
                    response.AddFailureMessage(FailureMessageType.Business, "Tree not found");

                    return response;
                }

                var child = request.Map();

                if (null == tree.Children)
                {
                    tree.Children = new List<TreeChild>();
                }

                tree.Children.Add(child);

                _repository.Update(tree);
            }
            catch (Exception uhEx)
            {
                response.AddFailureMessage(FailureMessageType.Critical, uhEx.Message);
            }

            return response;
        }
    }
}