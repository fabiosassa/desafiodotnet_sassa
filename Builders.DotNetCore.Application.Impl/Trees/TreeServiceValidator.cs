﻿using System;

using Builders.DotNetCore.Services.Enums;

using Builders.DotNetCore.Application.Trees;
using Builders.DotNetCore.Application.Trees.Messages;

namespace Builders.DotNetCore.Application.Impl.Trees
{
    public class TreeServiceValidator : ITreeServiceValidator
    {
        public TreeInsertResponse Validate(TreeInsertRequest request)
        {
            var response = new TreeInsertResponse();

            if (null == request)
            {
                response.AddFailureMessage(FailureMessageType.Validation, "Invalid request");

                return response;
            }

            if (String.IsNullOrWhiteSpace(request.Id))
            {
                response.AddFailureMessage(FailureMessageType.Validation, "The id is required");

                return response;
            }

            return response;
        }

        public TreeUpdateResponse Validate(TreeUpdateRequest request)
        {
            var response = new TreeUpdateResponse();

            if (null == request)
            {
                response.AddFailureMessage(FailureMessageType.Validation, "Invalid request");

                return response;
            }

            if (String.IsNullOrWhiteSpace(request.TreeId))
            {
                response.AddFailureMessage(FailureMessageType.Validation, "The treeid is required");

                return response;
            }

            return response;
        }
    }
}