﻿using System;
using System.Linq;

using Builders.DotNetCore.Domain;

using Builders.DotNetCore.Application.Trees.Messages;
using System.Collections.Generic;

namespace Builders.DotNetCore.Application.Impl.Trees
{
    public static class TreeServiceMapper
    {
        public static TreeDto Map(this Tree entity)
        {
            return new TreeDto { Id = entity.Id, Value = entity.Value, Children = entity.Children.Map() };
        }

        public static List<TreeChildDto> Map(this List<TreeChild> list)
        {
            if (null == list)
            {
                return null;
            }

            return list.Select(item => item.Map()).ToList();
        }

        public static TreeChildDto Map(this TreeChild entity)
        {
            if (null == entity)
            {
                return null;
            }

            var child = new TreeChildDto { Value = entity.Value };

            if (null != entity.Children && 0 < entity.Children.Count)
            {
                child.Children = entity.Children.Map();
            }

            return child;
        }

        public static Tree Map(this TreeInsertRequest request)
        {
            return new Tree { Id = request.Id, Value = request.Value, Children = request.Children.Map() };
        }

        public static TreeChild Map(this TreeUpdateRequest request)
        {
            return new TreeChild { Value = request.Value, Children = request.Children.Map() };
        }

        public static List<TreeChild> Map(this List<TreeChildDto> list)
        {
            if (null == list)
            {
                return null;
            }

            return list.Select(item => item.Map()).ToList();
        }

        public static TreeChild Map(this TreeChildDto entity)
        {
            if (null == entity)
            {
                return null;
            }

            var child = new TreeChild { Value = entity.Value };

            if (null != entity.Children && 0 < entity.Children.Count)
            {
                child.Children = entity.Children.Map();
            }

            return child;
        }
    }
}