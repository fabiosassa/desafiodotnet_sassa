﻿using System;

using Builders.DotNetCore.Services.Enums;

using Builders.DotNetCore.Domain.Palindromes;

using Builders.DotNetCore.Application.Palindromes;
using Builders.DotNetCore.Application.Palindromes.Messages;

namespace Builders.DotNetCore.Application.Impl.Palindromes
{
    public class PalindromeService : IPalindromeService
    {
        private readonly IPalindromeServiceValidator _validator;

        public PalindromeService(IPalindromeServiceValidator validator)
        {
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
        }

        public PalindromeResponse CheckWhetherPalindrome(PalindromeRequest request)
        {
            var response = _validator.Validate(request);

            if (!response.IsValid)
            {
                return response;
            }

            try
            {
                var entity = new Palindrome(request.Word);

                response.Word = entity.Word;
                response.IsPalindrome = entity.IsPalindrome;
            }
            catch (Exception uhEx)
            {
                response.AddFailureMessage(FailureMessageType.Critical, uhEx.Message);
            }

            return response;
        }
    }
}