﻿using System;

using Builders.DotNetCore.Services.Enums;

using Builders.DotNetCore.Application.Palindromes;
using Builders.DotNetCore.Application.Palindromes.Messages;

namespace Builders.DotNetCore.Application.Impl.Palindromes
{
    public class PalindromeServiceValidator : IPalindromeServiceValidator
    {
        public PalindromeResponse Validate(PalindromeRequest request)
        {
            var response = new PalindromeResponse();

            if (null == request || String.IsNullOrEmpty(request.Word))
            {
                response.AddFailureMessage(FailureMessageType.Validation, "Invalid request");

                return response;
            }

            if (String.Empty == request.Word.Trim())
            {
                response.AddFailureMessage(FailureMessageType.Validation, "The word is required");

                return response;
            }

            if (3 > request.Word.Length)
            {
                response.AddFailureMessage(FailureMessageType.Validation, "The word length is invalid. Min: 3 letters");

                return response;
            }

            return response;
        }
    }
}