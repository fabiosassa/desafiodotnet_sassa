﻿using System;

using Builders.DotNetCore.Application.Palindromes.Messages;

namespace Builders.DotNetCore.Application.Palindromes
{
    public interface IPalindromeServiceValidator
    {
        PalindromeResponse Validate(PalindromeRequest request);
    }
}