﻿using System;

using Builders.DotNetCore.Application.Palindromes.Messages;

namespace Builders.DotNetCore.Application.Palindromes
{
    public interface IPalindromeService
    {
        PalindromeResponse CheckWhetherPalindrome(PalindromeRequest request);
    }
}