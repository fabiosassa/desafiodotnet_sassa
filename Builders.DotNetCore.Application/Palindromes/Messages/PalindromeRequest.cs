﻿using System;
using System.Runtime.Serialization;

using Builders.DotNetCore.Services.Http;

namespace Builders.DotNetCore.Application.Palindromes.Messages
{
    [DataContract(Namespace = "https://builders.dotnetcore/sassa/types")]
    public class PalindromeRequest : SSTIRequest
    {
        [DataMember]
        public string Word { get; set; }
    }
}