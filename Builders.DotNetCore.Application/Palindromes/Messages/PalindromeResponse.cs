﻿using System;
using System.Runtime.Serialization;

using Builders.DotNetCore.Services.Http;

namespace Builders.DotNetCore.Application.Palindromes.Messages
{
    [DataContract(Namespace = "https://builders.dotnetcore/sassa/types")]
    public class PalindromeResponse : SSTIResponse
    {
        [DataMember]
        public string Word { get; set; }
        [DataMember]
        public bool IsPalindrome { get; set; }
    }
}