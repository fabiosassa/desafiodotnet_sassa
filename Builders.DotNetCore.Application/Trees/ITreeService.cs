﻿using System;

using Builders.DotNetCore.Application.Trees.Messages;

namespace Builders.DotNetCore.Application.Trees
{
    public interface ITreeService
    {
        TreeSelectResponse Select();
        TreeInsertResponse Insert(TreeInsertRequest request);
        TreeUpdateResponse Update(TreeUpdateRequest request);
    }
}