﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Builders.DotNetCore.Services.Http;

namespace Builders.DotNetCore.Application.Trees.Messages
{
    [DataContract(Namespace = "https://builders.dotnetcore/sassa/types")]
    public class TreeUpdateRequest : SSTIRequest
    {
        public string TreeId { get; set; }
        [DataMember]
        public int Value { get; set; }
        [DataMember]
        public List<TreeChildDto> Children { get; set; }
    }
}