﻿using System;
using System.Runtime.Serialization;

using Builders.DotNetCore.Services.Http;

namespace Builders.DotNetCore.Application.Trees.Messages
{
    [DataContract(Namespace = "https://builders.dotnetcore/sassa/types")]
    public class TreeUpdateResponse : SSTIResponse
    {
    }
}