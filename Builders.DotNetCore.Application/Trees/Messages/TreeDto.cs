﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Builders.DotNetCore.Application.Trees.Messages
{
    [DataContract(Namespace = "https://builders.dotnetcore/sassa/types")]
    public class TreeDto
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public int Value { get; set; }
        [DataMember]
        public List<TreeChildDto> Children { get; set; }
    }
}