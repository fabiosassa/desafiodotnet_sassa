﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Builders.DotNetCore.Services.Http;

namespace Builders.DotNetCore.Application.Trees.Messages
{
    [DataContract(Namespace = "https://builders.dotnetcore/sassa/types")]
    public class TreeSelectResponse : SSTIResponse
    {
        [DataMember]
        public List<TreeDto> Trees { get; set; }
    }
}