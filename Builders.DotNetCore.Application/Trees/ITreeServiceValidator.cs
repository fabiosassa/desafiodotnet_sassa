﻿using System;

using Builders.DotNetCore.Application.Trees.Messages;

namespace Builders.DotNetCore.Application.Trees
{
    public interface ITreeServiceValidator
    {
        TreeInsertResponse Validate(TreeInsertRequest request);
        TreeUpdateResponse Validate(TreeUpdateRequest request);
    }
}