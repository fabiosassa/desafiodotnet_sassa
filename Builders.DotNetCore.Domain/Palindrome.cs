﻿using System;
using System.Linq;

namespace Builders.DotNetCore.Domain.Palindromes
{
    public class Palindrome
    {
        public Palindrome(string word)
        {
            Word = word ?? throw new ArgumentNullException(nameof(word));
        }

        public string Word { get; private set; }

        public bool IsPalindrome
        {
            get
            {
                if (3 > Word.Length)
                {
                    return false;
                }

                string wordLowerCase = Word.ToLower();

                for (int e = 0, d = wordLowerCase.Length - 1; e < d; e++, d--)
                {
                    // Caso seja necessário ignorar pontuação, espaços e permitir frases... descomentar o condicional abaixo
                    /*if (!char.IsLetterOrDigit(wordLowerCase[e]))
                    {
                        d++;
                    }
                    else if (!char.IsLetterOrDigit(wordLowerCase[d]))
                    {
                        e--;
                    }
                    else */
                    if (wordLowerCase[e] != wordLowerCase[d])
                    {
                        return false;
                    }
                }

                return true;
            }
        }
        //Word == String.Join(String.Empty, Word.Reverse());
    }
}