﻿using System;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain
{
    public class Tree
    {
        public string Id { get; set; }
        public int Value { get; set; }
        public List<TreeChild> Children { get; set; }
    }

    public class TreeChild
    {
        public int Value { get; set; }
        public List<TreeChild> Children { get; set; }
    }
}