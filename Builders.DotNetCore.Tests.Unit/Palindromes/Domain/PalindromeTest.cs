﻿using System;

using Xunit;

using Builders.DotNetCore.Domain.Palindromes;

namespace Builders.DotNetCore.Tests.Unit.Palindromes.Domain
{
    public class PalindromeTest
    {
        [Fact]
        public void WordMissing()
        {
            Assert.Throws<ArgumentNullException>(() => new Palindrome(null));
        }

        [Fact]
        public void IsNotPalindrome1Chr()
        {
            var palindrome = new Palindrome("a");

            Assert.False(palindrome.IsPalindrome);
        }

        [Fact]
        public void IsNotPalindromeMoreThan3Chrs()
        {
            var palindrome = new Palindrome("banana");

            Assert.False(palindrome.IsPalindrome);
        }

        [Fact]
        public void IsPalindrome()
        {
            var palindrome = new Palindrome("ana");

            Assert.True(palindrome.IsPalindrome);
        }

        [Fact]
        public void GetWord()
        {
            string word = "arara";

            var palindrome = new Palindrome(word);

            Assert.Equal(word, palindrome.Word);
        }
    }
}