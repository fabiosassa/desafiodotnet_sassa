﻿using System;

using Xunit;

using Builders.DotNetCore.Application.Palindromes;
using Builders.DotNetCore.Application.Impl.Palindromes;
using Builders.DotNetCore.Application.Palindromes.Messages;

namespace Builders.DotNetCore.Tests.Unit.Palindromes.Services
{
    public class PalindromeServiceValidatorTest
    {
        private IPalindromeServiceValidator _validator;

        public PalindromeServiceValidatorTest()
        {
            _validator = new PalindromeServiceValidator();
        }

        [Fact]  
        public void InvalidRequest()
        {
            PalindromeRequest request = null;

            var response = _validator.Validate(request);

            Assert.False(response.IsValid);
        }

        [Fact]
        public void InvalidRequestWordNull()
        {
            PalindromeRequest request = new PalindromeRequest { Word = null };

            var response = _validator.Validate(request);

            Assert.False(response.IsValid);
        }

        [Fact]
        public void InvalidRequestWordEmpty()
        {
            PalindromeRequest request = new PalindromeRequest { Word = String.Empty };

            var response = _validator.Validate(request);

            Assert.False(response.IsValid);
        }

        [Fact]
        public void InvalidRequestWordLengthLessThan3()
        {
            PalindromeRequest request = new PalindromeRequest { Word = "ab" };

            var response = _validator.Validate(request);

            Assert.False(response.IsValid);
        }

        [Fact]
        public void ValidRequest()
        {
            PalindromeRequest request = new PalindromeRequest { Word = "abc" };

            var response = _validator.Validate(request);

            Assert.True(response.IsValid);
        }
    }
}