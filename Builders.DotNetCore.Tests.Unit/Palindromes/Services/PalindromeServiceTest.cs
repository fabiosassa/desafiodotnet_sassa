﻿using System;

using Xunit;

using Builders.DotNetCore.Application.Palindromes;
using Builders.DotNetCore.Application.Impl.Palindromes;
using Builders.DotNetCore.Application.Palindromes.Messages;

namespace Builders.DotNetCore.Tests.Unit.Palindromes.Services
{
    public class PalindromeServiceTest
    {
        private IPalindromeService _service;

        public PalindromeServiceTest()
        {
            _service = new PalindromeService(new Application.Impl.Palindromes.PalindromeServiceValidator());
        }

        [Fact]
        public void InvalidRequest()
        {
            PalindromeRequest request = null;

            var response = _service.CheckWhetherPalindrome(request);

            Assert.False(response.IsValid);
        }

        [Fact]
        public void InvalidRequestWithMessageList()
        {
            PalindromeRequest request = null;

            var response = _service.CheckWhetherPalindrome(request);

            Assert.NotEmpty(response.Messages);
        }

        [Fact]
        public void ValidRequest()
        {
            PalindromeRequest request = new PalindromeRequest { Word = "Teste" };

            var response = _service.CheckWhetherPalindrome(request);

            Assert.True(response.IsValid);
        }

        [Fact]
        public void InvalidRequestWithEmptyMessageList()
        {
            PalindromeRequest request = new PalindromeRequest { Word = "Ana" };

            var response = _service.CheckWhetherPalindrome(request);

            Assert.Empty(response.Messages);
        }

        [Fact]
        public void IsPalindrome()
        {
            PalindromeRequest request = new PalindromeRequest { Word = "Arara" };

            var response = _service.CheckWhetherPalindrome(request);

            Assert.True(response.IsPalindrome);
        }

        [Fact]
        public void IsNotPalindrome()
        {
            PalindromeRequest request = new PalindromeRequest { Word = "Anne" };

            var response = _service.CheckWhetherPalindrome(request);

            Assert.False(response.IsPalindrome);
        }
    }
}