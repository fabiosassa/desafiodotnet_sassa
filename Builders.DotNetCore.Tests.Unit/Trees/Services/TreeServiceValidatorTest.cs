﻿using System;

using Xunit;

using Builders.DotNetCore.Application.Trees;
using Builders.DotNetCore.Application.Impl.Trees;
using Builders.DotNetCore.Application.Trees.Messages;
using System.Collections.Generic;

namespace Builders.DotNetCore.Tests.Unit.Trees.Services
{
    public class TreeServiceValidatorTest
    {
        private ITreeServiceValidator _validator;

        public TreeServiceValidatorTest()
        {
            _validator = new TreeServiceValidator();
        }

        #region "[ INSERT ]"

        [Fact]
        public void InvalidInsertRequest()
        {
            TreeInsertRequest request = null;

            var response = _validator.Validate(request);

            Assert.False(response.IsValid);
        }

        [Fact]
        public void InvalidInsertRequestWithoutTreeId()
        {
            var request = new TreeInsertRequest();

            var response = _validator.Validate(request);

            Assert.False(response.IsValid);
        }

        [Fact]
        public void ValidInsertRequestWithoutChildren()
        {
            var request = new TreeInsertRequest { Id = "1", Value = 5, Children = null };

            var response = _validator.Validate(request);

            Assert.True(response.IsValid);
        }

        [Fact]
        public void ValidInsertRequestWithChildren()
        {
            var request = new TreeInsertRequest { Id = "1", Value = 5, Children = new List<TreeChildDto> { new TreeChildDto { Value = 51, Children = null  } } };

            var response = _validator.Validate(request);

            Assert.True(response.IsValid);
        }

        #endregion

        #region "[ UPDATE ]"

        [Fact]
        public void InvalidUpdateRequest()
        {
            TreeUpdateRequest request = null;

            var response = _validator.Validate(request);

            Assert.False(response.IsValid);
        }

        [Fact]
        public void InvalidUpdateRequestWithoutTreeId()
        {
            var request = new TreeUpdateRequest();

            var response = _validator.Validate(request);

            Assert.False(response.IsValid);
        }

        [Fact]
        public void ValidUpdateRequestWithoutChildren()
        {
            var request = new TreeUpdateRequest { TreeId = "1", Value = 5, Children = null };

            var response = _validator.Validate(request);

            Assert.True(response.IsValid);
        }

        [Fact]
        public void ValidUpdateRequestWithChildren()
        {
            var request = new TreeUpdateRequest { TreeId = "1", Value = 5, Children = new List<TreeChildDto> { new TreeChildDto { Value = 51, Children = null } } };

            var response = _validator.Validate(request);

            Assert.True(response.IsValid);
        }

        #endregion

    }
}