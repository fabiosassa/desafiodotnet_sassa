﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xunit;

using Builders.DotNetCore.Application.Trees;
using Builders.DotNetCore.Application.Impl.Palindromes;
using Builders.DotNetCore.Application.Impl.Trees;
using Moq;
using Builders.DotNetCore.Data;

namespace Builders.DotNetCore.Tests.Unit.Trees.Services
{
    public class TreeServiceTest
    {
        private readonly ITreeServiceValidator _validator;

        public TreeServiceTest()
        {
            _validator = new TreeServiceValidator();
        }

        #region "[ SELECT ]"

        [Fact]
        public void SelectResponseWithNoTreeWasFound()
        {
            var uowMock = new Mock<IUnitOfWork>() ;
            var repoFake = new Mock<Repository<Domain.Tree>>();

            repoFake.Setup(s => s.ToList()).Returns(Enumerable.Empty<Domain.Tree>());
            uowMock .Setup(s => s.GetRepository<Domain.Tree>()).Returns(repoFake.Object);

            var service = new TreeService(uowMock.Object, _validator);

            var response = service.Select();

            Assert.False(response.IsValid);
            Assert.Null(response.Trees);
        }

        [Fact]
        public void SelectResponseWithException()
        {
            var uowMock = new Mock<IUnitOfWork>();
            var repoFake = new Mock<Repository<Domain.Tree>>();

            repoFake.Setup(s => s.ToList()).Throws(new NullReferenceException());
            uowMock.Setup(s => s.GetRepository<Domain.Tree>()).Returns(repoFake.Object);

            var service = new TreeService(uowMock.Object, _validator);

            var response = service.Select();

            Assert.False(response.IsValid);
            Assert.Null(response.Trees);
            Assert.Contains(response.Messages, (m) => m.Type == DotNetCore.Services.Enums.FailureMessageType.Critical);
        }

        [Fact]
        public void SelectResponseWithTwoTrees()
        {
            var uowMock = new Mock<IUnitOfWork>() ;
            var repoFake = new Mock<Repository<Domain.Tree>>();

            repoFake.Setup(s => s.ToList()).Returns(Enumerable.Repeat(new Domain.Tree(), 2));
            uowMock .Setup(s => s.GetRepository<Domain.Tree>()).Returns(repoFake.Object);

            var service = new TreeService(uowMock.Object, _validator);

            var response = service.Select();

            Assert.True(response.IsValid);
            Assert.NotEmpty(response.Trees);
            Assert.Equal(2, response.Trees.Count);
        }

        #endregion

        #region "[ INSERT ]"

        [Fact]
        public void InsertResponseWithValidationError()
        {
            var uowMock = new Mock<IUnitOfWork>();
            var repoFake = new Mock<Repository<Domain.Tree>>();

            repoFake.Setup(s => s.Add(It.IsAny<Domain.Tree>())).Throws(new NullReferenceException());
            uowMock.Setup(s => s.GetRepository<Domain.Tree>()).Returns(repoFake.Object);

            var service = new TreeService(uowMock.Object, _validator);

            var response = service.Insert(new Application.Trees.Messages.TreeInsertRequest { Id = "", Value = 1 });

            Assert.False(response.IsValid);
            Assert.Null(response.Tree);
            Assert.Contains(response.Messages, (m) => m.Type == DotNetCore.Services.Enums.FailureMessageType.Validation);
        }

        [Fact]
        public void InsertResponseWithException()
        {
            var uowMock = new Mock<IUnitOfWork>();
            var repoFake = new Mock<Repository<Domain.Tree>>();

            repoFake.Setup(s => s.Add(It.IsAny<Domain.Tree>())).Throws(new NullReferenceException());
            uowMock.Setup(s => s.GetRepository<Domain.Tree>()).Returns(repoFake.Object);

            var service = new TreeService(uowMock.Object, _validator);

            var response = service.Insert(new Application.Trees.Messages.TreeInsertRequest { Id = "1", Value = 1 });

            Assert.False(response.IsValid);
            Assert.Null(response.Tree);
            Assert.Contains(response.Messages, (m) => m.Type == DotNetCore.Services.Enums.FailureMessageType.Critical);
        }

        [Fact]
        public void InsertResponseSuccess()
        {
            var uowMock = new Mock<IUnitOfWork>();
            var repoFake = new Mock<Repository<Domain.Tree>>();

            repoFake.Setup(s => s.Add(It.IsAny<Domain.Tree>()));
            uowMock.Setup(s => s.GetRepository<Domain.Tree>()).Returns(repoFake.Object);

            var service = new TreeService(uowMock.Object, _validator);

            var request = new Application.Trees.Messages.TreeInsertRequest { Id = "1", Value = 1 };

            var response = service.Insert(new Application.Trees.Messages.TreeInsertRequest { Id = "1", Value = 1 });

            Assert.True(response.IsValid);
            Assert.NotNull(response.Tree);
            Assert.Equal(request.Id, response.Tree.Id);
            Assert.Equal(request.Value, response.Tree.Value);
        }

        #endregion

        #region "[ UPDATE ]"

        [Fact]
        public void UpdateResponseWithValidationError()
        {
            var uowMock = new Mock<IUnitOfWork>();
            var repoFake = new Mock<Repository<Domain.Tree>>();

            string treeId = new Random().Next(1,100).ToString();

            repoFake.Setup(s => s.Find(It.Is<string>(p => p == treeId))).Returns(new Domain.Tree());
            repoFake.Setup(s => s.Update(It.IsAny<Domain.Tree>()));
            uowMock.Setup(s => s.GetRepository<Domain.Tree>()).Returns(repoFake.Object);

            var service = new TreeService(uowMock.Object, _validator);

            var request = new Application.Trees.Messages.TreeUpdateRequest { TreeId = "", Value = 1 };

            var response = service.Update(request);

            Assert.False(response.IsValid);
            Assert.Contains(response.Messages, (m) => m.Type == DotNetCore.Services.Enums.FailureMessageType.Validation);
        }

        [Fact]
        public void UpdateResponseWithException()
        {
            var uowMock = new Mock<IUnitOfWork>();
            var repoFake = new Mock<Repository<Domain.Tree>>();

            string treeId = new Random().Next(1, 100).ToString();

            repoFake.Setup(s => s.Find(It.Is<string>(p => p == treeId))).Returns(new Domain.Tree());
            repoFake.Setup(s => s.Update(It.IsAny<Domain.Tree>())).Throws(new NullReferenceException());
            uowMock.Setup(s => s.GetRepository<Domain.Tree>()).Returns(repoFake.Object);

            var service = new TreeService(uowMock.Object, _validator);

            var request = new Application.Trees.Messages.TreeUpdateRequest { TreeId = treeId, Value = 1 };

            var response = service.Update(request);

            Assert.False(response.IsValid);
            Assert.Contains(response.Messages, (m) => m.Type == DotNetCore.Services.Enums.FailureMessageType.Critical);
        }

        [Fact]
        public void UpdateResponseTreeNotFound()
        {
            var uowMock = new Mock<IUnitOfWork>();
            var repoFake = new Mock<Repository<Domain.Tree>>();

            string treeId = new Random().Next(1, 100).ToString();

            repoFake.Setup(s => s.Find(It.Is<string>(p => p == treeId))).Returns<Domain.Tree>(null);
            repoFake.Setup(s => s.Update(It.IsAny<Domain.Tree>()));
            uowMock.Setup(s => s.GetRepository<Domain.Tree>()).Returns(repoFake.Object);

            var service = new TreeService(uowMock.Object, _validator);

            var request = new Application.Trees.Messages.TreeUpdateRequest { TreeId = treeId, Value = 1 };

            var response = service.Update(request);

            Assert.False(response.IsValid);
            Assert.Contains(response.Messages, (m) => m.Type == DotNetCore.Services.Enums.FailureMessageType.Business);
        }

        [Fact]
        public void UpdateResponseSuccess()
        {
            var uowMock = new Mock<IUnitOfWork>();
            var repoFake = new Mock<Repository<Domain.Tree>>();

            string treeId = new Random().Next(1, 100).ToString();

            repoFake.Setup(s => s.Find(It.Is<string>(p => p == treeId))).Returns(new Domain.Tree { Id = treeId });
            repoFake.Setup(s => s.Update(It.IsAny<Domain.Tree>()));
            uowMock.Setup(s => s.GetRepository<Domain.Tree>()).Returns(repoFake.Object);

            var service = new TreeService(uowMock.Object, _validator);

            var request = new Application.Trees.Messages.TreeUpdateRequest { TreeId = treeId, Value = 1 };

            var response = service.Update(request);

            Assert.True(response.IsValid);
        }

        #endregion
    }
}