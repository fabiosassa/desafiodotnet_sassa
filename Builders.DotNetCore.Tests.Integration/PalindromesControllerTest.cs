using Builders.DotNetCore.Application.Palindromes.Messages;
using Builders.DotNetCore.Tests.Integration.Infraestrutura;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Builders.DotNetCore.Tests.Integration
{
    public class PalindromesControllerTest : TestBase
    {
        public PalindromesControllerTest(TestFixture<TestStartup> fixture)
            : base(fixture)
        {
        }

        [Fact]
        public async void GetPalindromeWord()
        {
            var response = await base.GetAsync<PalindromeResponse>("palindromes/isPalindrome?word=arara");

            Assert.True(response.IsPalindrome);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async void GetNotPalindromeWord()
        {
            var response = await base.GetAsync<PalindromeResponse>("palindromes/isPalindrome?word=teste");

            Assert.False(response.IsPalindrome);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
