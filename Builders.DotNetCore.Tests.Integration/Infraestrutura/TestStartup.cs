﻿using Builders.DotNetCore.Data;
using Builders.DotNetCore.WebApi;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Builders.DotNetCore.Tests.Integration.Infraestrutura
{
    public class TestStartup : Startup
    {
        public TestStartup(IHostingEnvironment env)
            : base(env)
        {
        }

        protected override void ConfigurarDatabase(IServiceCollection services)
        {
            services.AddSingleton<IUnitOfWork, TestMongoUnitOfWork>();
        }
    }
}
