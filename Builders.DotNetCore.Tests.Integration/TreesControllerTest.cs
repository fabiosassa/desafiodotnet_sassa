using System;
using System.Net;

using Xunit;

using Builders.DotNetCore.Application.Trees.Messages;
using Builders.DotNetCore.Tests.Integration.Infraestrutura;

namespace Builders.DotNetCore.Tests.Integration
{
    public class TreesControllerTest : TestBase
    {
        public TreesControllerTest(TestFixture<TestStartup> fixture)
            : base(fixture)
        {
        }

        #region "[ GET ]"

        [Fact]
        public async void GetTrees()
        {
            var insertResponse = await base.PostAsync<TreeInsertRequest, TreeInsertResponse>("trees", new TreeInsertRequest { Id = "1", Value = 1 });

            Assert.True(insertResponse.IsValid);

            var selectResponse = await base.GetAsync<TreeSelectResponse>("trees");

            Assert.NotEmpty(selectResponse.Trees);
        }

        #endregion

        #region "[ POST ]"

        [Fact]
        public async void PostTreesWithValidationError()
        {
            var response = await base.PostAsync<TreeInsertRequest, TreeInsertResponse>("trees", new TreeInsertRequest { Id = "", Value = 1 });

            Assert.False(response.IsValid);
        }

        [Fact]
        public async void PostTrees()
        {
            var response = await base.PostAsync<TreeInsertRequest, TreeInsertResponse>("trees", new TreeInsertRequest { Id = "2", Value = 1 });

            Assert.True(response.IsValid);
            Assert.NotNull(response.Tree);
        }

        #endregion

        #region "[ UPDATE ]"

        [Fact]
        public async void PatchTreesWithValidationError()
        {
            var patchResponse = await base.PatchAsync<TreeUpdateRequest, TreeUpdateResponse>("trees/10", new TreeUpdateRequest { TreeId = "", Value = 0 });

            Assert.False(patchResponse.IsValid);
        }

        [Fact]
        public async void PatchTrees()
        {
            string treeId = "9";

            var insertResponse = await base.PostAsync<TreeInsertRequest, TreeInsertResponse>("trees", new TreeInsertRequest { Id = treeId, Value = 1 });

            Assert.True(insertResponse.IsValid);

            var patchResponse = await base.PatchAsync<TreeUpdateRequest, TreeUpdateResponse>($"trees/{treeId}", new TreeUpdateRequest { Value = 2 });

            Assert.True(patchResponse.IsValid);
        }

        #endregion
    }
}
